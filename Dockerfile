FROM docker.io/node:14.20.1-buster-slim

# Wekan image for OpenShift Origin

ENV DEBIAN_FRONTEND=noninteractive \
    NODE_ENV=production \
    OPENSSL_CONF=/etc/ssl \
    WEKAN_REPOSITORY=https://github.com/wekan/wekan.git \
    WK_VERSION=6.51

LABEL io.k8s.description="Wekan is a NodeJS & MongoDB based Kanban solution." \
      io.k8s.display-name="Wekan $WK_VERSION" \
      io.openshift.expose-services="3000:http" \
      io.openshift.tags="kanban,wekan" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-wekan" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$WK_VERSION"

USER root
COPY config/* /

RUN set -x \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Wekan Dependencies" \
    && apt-get install --no-install-recommends --no-install-suggests -y \
	build-essential git libnss-wrapper apt-utils python3 python3-dev \
	bsdtar gnupg wget curl bzip2 ca-certificates gcc-8 python3-setuptools \
	libfontconfig libssl-dev python3-pip python3-wheel \
    && pip3 install --upgrade setuptools \
    && pip3 install esprima \
    && echo "# Install Wekan" \
    && mkdir -p /wekan \
    && git clone --depth 1 --branch "v$WK_VERSION" "$WEKAN_REPOSITORY" /wekan/app \
    && cd /wekan/app \
    && git log --pretty=format:'%ad %h %d' --abbrev-commit --date=short -1 \
    && mv /bin/tar /bin/tar.orig \
    && ln -sf /usr/bin/bsdtar /bin/tar \
    && mkdir -p /wekan/app/packages /wekan/python /wekan/app/public/api \
    && curl https://install.meteor.com -o /wekan/install_meteor.sh \
    && sed -i 's/VERBOSITY="--silent"/VERBOSITY="--progress-bar"/' /wekan/install_meteor.sh \
    && chmod +x /wekan/install_meteor.sh \
    && ( \
	    cd /wekan \
	    && echo "# Install Meteor" \
	    && /wekan/install_meteor.sh \
	    && cd /wekan/app \
	    && echo "# Install Meteor Packages" \
	    && npm install --unsafe-perm \
	    && /root/.meteor/meteor --allow-superuser build --directory /wekan/app_build \
	    && echo "# Install Wekan Packages" \
	    && cd /wekan/app_build/bundle/programs/server \
	    && npm install --unsafe-perm \
	    && cd node_modules/fibers \
	    && node build.js \
	    && rm -fr /wekan/app_build/bundle/programs/web.browser.legacy \
	    && mv /wekan/app_build/bundle /build; \
	) \
    && mv /reset-tls.sh /usr/local/bin/ \
    && echo "# Fixing permissions" \
    && chown -R 1001:root /wekan /build /etc/ssl /usr/local/share/ca-certificates \
    && chmod -R g=u /wekan /build /etc/ssl /usr/local/share/ca-certificates \
    && mkdir -p /run \
    && ( chmod -R g=u /run /tmp || echo nevermind ) \
    && echo "# Cleanup Image" \
    && rm -f /bin/tar \
    && mv /bin/tar.orig /bin/tar \
    && apt-get remove --purge -y --auto-remove apt-utils bsdtar gnupg git \
	build-essential python python3 wget bzip2 gcc-8 python-dev python3-dev \
	libfontconfig libfontconfig1 libssl-dev \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get autoremove -y \
    && apt-get clean \
    && npm uninstall -g api2html \
    && rm -fr /var/lib/apt/lists/* /root/.meteor /wekan /tmp/* /usr/share/doc \
	/usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

WORKDIR /build
USER 1001
ENTRYPOINT ["/run-wekan.sh"]
CMD ["node", "main.js"]
