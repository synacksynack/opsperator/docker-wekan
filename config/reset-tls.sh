#!/bin/sh

should_rehash=false
if test -d /etc/pki/ca-trust/source/anchors; then
    dir=/etc/pki/ca-trust/source/anchors
else
    dir=/usr/local/share/ca-certificates
fi
if test "$LDAP_HOST" -a "$LDAP_IS_IPA"; then
    if ! curl --connect-timeout 5 http://$LDAP_HOST/ipa/config/ca.crt \
	    -o $dir/ipa.crt; then
	rm -f $dir/ipa.crt
    else
	export "LDAP_CA_CERT=$(cat $dir/ipa.crt)"
	export "OAUTH2_CA_CERT=$dir/ipa.crt"
    fi
fi
for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
do
    if test -s $f; then
	d=`echo $f | sed 's|/|-|g'`
	if ! test -s $dir/kube$d; then
	    if ! cat $f >$dir/kube$d; then
		echo WARNING: failed installing $f certificate authority >&2
	    else
		if test -z "$LDAP_CA_CERT"; then
		    export "LDAP_CA_CERT=$(cat $dir/kube$d)"
		fi
		should_rehash=true
	    fi
	fi
    fi
done
if $should_rehash; then
    if test -d /etc/pki/ca-trust/source/anchors; then
	if ! update-ca-trust; then
	    echo WARNING: failed updating trusted certificate authorities >&2
	fi
    elif ! update-ca-certificates; then
	echo WARNING: failed updating trusted certificate authorities >&2
    fi
fi
unset should_rehash CA_CHECK dir
