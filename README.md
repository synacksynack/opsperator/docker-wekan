# k8s Wekan

Kubernetes Wekan image

Based on https://github.com/wekan/wekan

Build with:

```
$ make build
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name                                    |    Description                    | Default                                                     |
| :-------------------------------------------------- | --------------------------------- | ----------------------------------------------------------- |
|  `ACCOUNTS_LOCKOUT_KNOWN_USERS_FAILURES_BEFORE`     | Lockout Known Users After         | `3`                                                         |
|  `ACCOUNTS_LOCKOUT_KNOWN_USERS_FAILURE_WINDOW`      | Lockout Known Users Window        | `15` seconds                                                |
|  `ACCOUNTS_LOCKOUT_KNOWN_USERS_PERIOD`              | Lockout Known Users Period        | `60` seconds                                                |
|  `ACCOUNTS_LOCKOUT_UNKNOWN_USERS_FAILURES_BERORE`   | Lockout Unknown Users After       | `3`                                                         |
|  `ACCOUNTS_LOCKOUT_UNKNOWN_USERS_FAILURE_WINDOW`    | Lockout Unknown Users Window      | `15` seconds                                                |
|  `ACCOUNTS_LOCKOUT_UNKNOWN_USERS_LOCKOUT_PERIOD`    | Lockout Unknown Users Period      | `60` seconds                                                |
|  `CUSTOM_LOGO`                                      | Custom Wekan Header Logo          | unset                                                       |
|  `LDAP_AUTHENTIFICATION`                            | Use OpenLDAP Authentification     | `true`                                                      |
|  `LDAP_AUTHENTIFICATION_PASSWORD`                   | OpenLDAP Bind Password            | `secret`                                                    |
|  `LDAP_BACKGROUND_SYNC`                             | OpenLDAP Background Sync          | `true`                                                      |
|  `LDAP_BACKGROUND_SYNC_INTERVAL`                    | OpenLDAP Sync Interval            | `every 10 minutes`                                          |
|  `LDAP_BACKGROUND_SYNC_IMPORT_NEW_USERS`            | OpenLDAP Sync Import Users        | `true`                                                      |
|  `LDAP_BACKGROUND_SYNC_KEEP_EXISTANT_USERS_UPDATED` | OpenLDAP Sync Keep Local Editions | `true`                                                      |
|  `LDAP_BASEDN`                                      | OpenLDAP Base                     | unset                                                       |
|  `LDAP_BIND_RREFIX`                                 | OpenLDAP Bind DN Prefix           | `cn=wekan,ou=services`                                      |
|  `LDAP_CA_CERT`                                     | OpenLDAP CA Certificate           | unset, unless `LDAP_IS_IPA`                                 |
|  `LDAP_CONNECT_TIMEOUT`                             | OpenLDAP Connection Timeout       | `2000` ms                                                   |
|  `LDAP_DEFAULT_DOMAIN`                              | OpenLDAP Default Domain           | `demo.local`                                                |
|  `LDAP_GROUP_FILTER_ENABLE`                         | OpenLDAP Group Filters Toggle     | `true`                                                      |
|  `LDAP_GROUP_FILTER_OBJECTCLASS`                    | OpenLDAP Group Filter ObjectClass | `groupOfNames`                                              |
|  `LDAP_GROUP_FILTER_GROUP_ID_ATTRIBUTE`             | OpenLDAP Group ID Attribute       | `cn`                                                        |
|  `LDAP_GROUP_FILTER_GROUP_MEMBER_ATTRIBUTE`         | OpenLDAP Group Member Attribute   | `member`                                                    |
|  `LDAP_GROUP_FILTER_GROUP_MEMBER_FORMAT`            | OpenLDAP Group Member Format      | `dn`                                                        |
|  `LDAP_GROUP_FILTER_GROUP_NAME`                     | OpenLDAP Group Filter             | `*`                                                         |
|  `LDAP_EMAIL_FIELD`                                 | OpenLDAP Email Field              | `mail`                                                      |
|  `LDAP_EMAIL_MATCH_ENABLE`                          | OpenLDAP Email Match Toggle       | `false`                                                     |
|  `LDAP_EMAIL_MATCH_REQUIRE`                         | OpenLDAP Email Match Required     | `false`                                                     |
|  `LDAP_ENABLE`                                      | OpenLDAP Authentication Toggle    | `true` if `LDAP_BASEDN` set                                 |
|  `LDAP_ENCRYPTION`                                  | OpenLDAP Encryption               | unset                                                       |
|  `LDAP_FULLNAME_FIELD`                              | OpenLDAP Fullname Field           | `displayName` or `sn`                                       |
|  `LDAP_HOST`                                        | OpenLDAP Backend Address          | `127.0.0.1`                                                 |
|  `LDAP_IDLE_TIMEOUT`                                | OpenLDAP Innactivity Timeout      | `10000` ms                                                  |
|  `LDAP_IS_IPA`                                      | LDAP Backend is FreeIPA/IDM       | undef                                                       |
|  `LDAP_LOG_ENABLED`                                 | OpenLDAP Authentication Logs      | `false`                                                     |
|  `LDAP_LOGIN_FALLBACK`                              | OpenLDAP Login Fallback           | `false`                                                     |
|  `LDAP_PORT`                                        | OpenLDAP Port                     | `1389`                                                      |
|  `LDAP_RECONNECT`                                   | OpenLDAP Reconnect                | `true`                                                      |
|  `LDAP_REJECT_UNAUTHORIZED`                         | OpenLDAP Reject Invalid Certifs   | `true`                                                      |
|  `LDAP_SYNC_USER_DATA`                              | OpenLDAP Sync User Data           | `true`                                                      |
|  `LDAP_SYNC_USER_DATA_FIELDMAP`                     | OpenLDAP Sync Fields Map          | `{"cn":"name","mail":"email"}`                              |
|  `LDAP_TIMEOUT`                                     | OpenLDAP Timeout                  | `10000` ms                                                  |
|  `LDAP_UNIQUE_IDENTIFIER_FIELD`                     | OpenLDAP User Unique ID           | `entryUUID`                                                 |
|  `LDAP_USER_SEARCH_FIELD`                           | OpenLDAP User Search Field        | `uid`                                                       |
|  `LDAP_USER_SEARCH_FILTER`                          | OpenLDAP User Search Filter       | `(&(objectClass=inetOrgPerson)(!(pwdAccountLockedTime=*)))` |
|  `LDAP_USER_SEARCH_SCOPE`                           | OpenLDAP User Search Scope        | `sub`                                                       |
|  `LDAP_USERNAME_FIELD`                              | OpenLDAP Username Field           | `uid`                                                       |
|  `LDAP_UTF8_NAMES_SLUGIFY`                          | OpenLDAP UTF8 Slugify Toggle      | `true`                                                      |
|  `LEMON_PROTO`                                      | LemonLDAP Protocol                | undef                                                       |
|  `MONGODB_DATABASE`                                 | Wekan MongoDB Database            | `wekan`                                                     |
|  `MONGODB_HOST`                                     | Wekan MongoDB Host                | `mongob-wekan`                                              |
|  `MONGODB_PASSWORD`                                 | Wekan MongoDB Password            | `secret`                                                    |
|  `MONGODB_PORT`                                     | Wekan MongoDB Port                | `27017`                                                     |
|  `MONGODB_USER`                                     | Wekan MongoDB User                | `wekan`                                                     |
|  `PASSWORD_LOGIN_ENABLED`                           | Wekan Password login Toggle       | `true`                                                      |
|  `OAUTH2_AUTH_ENDPOINT`                             | Oauth2 Auth URI                   | `/oauth2/authorize`                                         |
|  `OAUTH2_CLIENT_ID`                                 | Oauth2 Client ID                  | `wekan` if `LEMON_PROTO`                                    |
|  `OAUTH2_ENABLED`                                   | Oauth2 Toggle                     | `true` if `LEMON_PROTO` set                                 |
|  `OAUTH2_ID_MAP`                                    | Oauth2 ID Map                     | `sub`                                                       |
|  `OAUTH2_SECRET`                                    | Oauth2 Secret                     | `wekan` if `LEMON_PROTO`                                    |
|  `OAUTH2_SERVER_URL`                                | Oauth2 Server URL                 | LLNG if `LEMON_PROTO`                                       |
|  `OAUTH2_TOKEN_ENDPOINT`                            | Oauth2 Token URI                  | `/oauth2/token`                                             |
|  `OAUTH2_USERINFO_ENDPOINT`                         | Oauth2 UserInfo URI               | `/oauth2/userinfo`                                          |
|  `PORT`                                             | Wekan HTTP Port                   | `8080`                                                      |
|  `WITH_API`                                         | Wekan API Toggle                  | `false`                                                     |
|  `WRITEABLE_PATH`                                   | Some path Wekan writes into       | `/data`                                                     |
